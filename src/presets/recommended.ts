import { RulesConfig, PresetOptions, PresetId } from "lintmyride";

export const presetId: PresetId = "github/recommended";

const rules: RulesConfig = {
  "github/issue-template": "warn",
};

export const preset: PresetOptions = {
  rules: rules,
};
