import { AvailablePresets, AvailableRules } from "lintmyride";
import * as issueTemplate from "./rules/issue-template";
import * as recommendedPreset from "./presets/recommended";

export const presets: AvailablePresets = [
  recommendedPreset,
];

export const rules: AvailableRules = [
  issueTemplate,
];
