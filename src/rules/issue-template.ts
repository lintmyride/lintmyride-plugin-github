import { isRootDir, CreateVisitors, DirNode } from "lintmyride";

export const ruleId = "github/issue-template";

export const createVisitors: CreateVisitors<DirNode> = (context) => {
  return {
    Dir: (node) => {
      if (
        (
          isRootDir(node) &&
          !node.children.includes(".github")
        ) ||
        ( node.path === ".github" && !node.children.includes("ISSUE_TEMPLATE") ) ||
        ( node.path === ".github/ISSUE_TEMPLATE" && node.children.length === 0)
      ) {
        return {
          output: "It is recommended to set up Issue Templates for your repository.",
          url: "https://docs.github.com/en/github/building-a-strong-community/configuring-issue-templates-for-your-repository",
        };
      }
    },
  };
};
