import { describe, it, expect } from "@jest/globals";
import { DirNode, VisitorContext, getEnterVisitor } from "lintmyride";
import { createVisitors } from "./issue-template";

describe("issue-template", () => {
  const mockContext: VisitorContext = {};

  it("rejects repositories without a .github folder", () => {
    const dirNode: DirNode = {
      type: "Dir",
      path: "",
      name: "arbitrary-dir",
      children: []
    };

    const visitors = createVisitors(mockContext);
    const dirVisitor = getEnterVisitor(visitors.Dir);

    expect(dirVisitor(dirNode)).toEqual({
      output: "It is recommended to set up Issue Templates for your repository.",
      url: "https://docs.github.com/en/github/building-a-strong-community/configuring-issue-templates-for-your-repository",
    });
  });

  it("rejects repositories without a .github/ISSUE_TEMPLATE/ folder", () => {
    const dirNode: DirNode = {
      type: "Dir",
      path: ".github",
      name: ".github",
      children: []
    };

    const visitors = createVisitors(mockContext);
    const dirVisitor = getEnterVisitor(visitors.Dir);

    expect(dirVisitor(dirNode)).toEqual({
      output: "It is recommended to set up Issue Templates for your repository.",
      url: "https://docs.github.com/en/github/building-a-strong-community/configuring-issue-templates-for-your-repository",
    });
  });

  it("rejects repositories with an empty .github/ISSUE_TEMPLATE/ folder", () => {
    const dirNode: DirNode = {
      type: "Dir",
      path: ".github/ISSUE_TEMPLATE",
      name: "ISSUE_TEMPLATE",
      children: []
    };

    const visitors = createVisitors(mockContext);
    const dirVisitor = getEnterVisitor(visitors.Dir);

    expect(dirVisitor(dirNode)).toEqual({
      output: "It is recommended to set up Issue Templates for your repository.",
      url: "https://docs.github.com/en/github/building-a-strong-community/configuring-issue-templates-for-your-repository",
    });
  });

  it("approves repositories with an issue template", () => {
    const dirNode: DirNode = {
      type: "Dir",
      path: ".github/ISSUE_TEMPLATE",
      name: "ISSUE_TEMPLATE",
      children: ["bug.md"]
    };

    const visitors = createVisitors(mockContext);
    const dirVisitor = getEnterVisitor(visitors.Dir);

    expect(dirVisitor(dirNode)).toBeUndefined();
  });
});
